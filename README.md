# 100 Days
## Chad Gueli

### Welcome
This group of repositories embodies my exploration of, practice with, and in some cases, attempts to improve SOTA deep learning techniques. The inital goal was to produce a model every day. I envisioned this goal as a means of developing my ability to rapidly prototype and produce a complete model. Towards that end I pumped out multiple CNNs and RNNs in a week for simple tasks, but ultimtely my improvement wasn't worth the time and money. As such, I switched over to practicing with and evaluating SOTA models. Unfortunately, not all of these notebooks will be open-sourced, but I have included links to those that are below.


### Computer Vision Models
**Day 002** Multi-Scale Feature Fusion for Gastro-Intestinal Segmentation

### Natural Language Models
**Day 003** Mini Language Models for Discrete Tasks

### Time-Series Models
**Day 004** Liquid NeuralODEs for ECG Diagnostics

### Graph Neural Networks
**Day 001** [Money-Saving HypergraphNN Collaborative Filtering](https://gitlab.com/100_Days/Day_1)

### Biomedical Models
**Day 002** Multi-Scale Feature Fusion for Gastro-Intestinal Segmentation

**Day 004** Liquid NeuralODEs for ECG Diagnostics

### Recomender Systems
**Day 001** [Money-Saving HypergraphNN Collaborative Filtering](https://gitlab.com/100_Days/Day_1)
